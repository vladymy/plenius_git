if (typeof jQuery !== 'undefined') {
  jQuery(function($) {
    "use strict";
    $(document).ready(function(){
      var home_slider=$('.home-slider .flexslider'),
          recent_projects__blog_posts__about_team__slider=$('.recent-projects .flexslider, .blog-posts .flexslider, .about-team .flexslider'),
          clients_slider=$('.clients .flexslider'),
          quotes_slider=$('.quotes .flexslider'),
          c_1_slider=$('.c-1 .flexslider'),
          latest_projects=$('.latest-projects .flexslider');

      home_slider.flexslider({
        animation: "slide",
        move: 1,
        directionNav: false
      });

      recent_projects__blog_posts__about_team__slider.flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 270,
        animationLoop: true,
        slideshow: false,
        move: 1
      });

      clients_slider.flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 150,
        animationLoop: true,
        slideshow: false
      });
      quotes_slider.flexslider({
          animation: "slide",
          controlNav: false,
          slideshow: false
      });
      c_1_slider.flexslider({
          animation: "slide",
          controlNav: false,
          slideshow: false
      });
      latest_projects.flexslider({
          animation: "slide",
          controlNav: false,
          slideshow: false
      });
    });
  });
}
;
