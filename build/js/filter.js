// DOMContentLoaded
$(document).ready(function() {
  "use strict";
  // get the first collection
  var applications = $('.portfolio-items');

  // clone applications to get a second collection
  var data = applications.clone();
  
  var filter__a = $('.filter a'),
      portfolio_items = $('.portfolio-items');

  filter__a.on('click', function(e) {
    var filteredData;
    filter__a.removeClass('active');
    $(this).addClass('active');
    if ($(this).data('value') === 'all') {
      filteredData = data.find('.item');
    } else {
      filteredData = data.find('.' + $(this).data('value'));
    }

    // finally, call quicksand

    portfolio_items.quicksand(filteredData, {
      duration: 800,
      easing: 'easeInOutQuad',
      adjustHeight: "auto",
      useScaling: true,
      attribute: 'id'
    });
    e.preventDefault();
  });
});
