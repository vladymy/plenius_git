function form_validate(form) {//Form validator
  "use strict";
  var isValidEmailAddress;
  isValidEmailAddress = function(emailAddress) {
    var pattern;
    pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
  };
  return form.find(".check").each(function() {
    var form_data;
    if ($(this).hasClass("email")) {
      form_data = $(this).val();
      if (isValidEmailAddress(form_data)) {
        $(this).removeClass("required");
        return $(this).addClass("passed");
      } else {
        $(this).removeClass("passed");
        $(this).addClass("required");
        $(this).animate({
          left: "-10px"
        }, 100).animate({
          left: "10px"
        }, 100);
        return $(this).animate({
          left: "0px"
        }, 100);
      }
    } else {
      if ($(this).val() === $(this).data('placeholder')) {
        $(this).removeClass("passed");
        $(this).addClass("required");
        $(this).animate({
          left: "-10px"
        }, 100).animate({
          left: "10px"
        }, 100);
        return $(this).animate({
          left: "0px"
        }, 100);
      } else {
        $(this).removeClass("required");
        return $(this).addClass("passed");
      }
    }
  });
}
function dropdown_menu(){  //dropdown menu show
  "use strict";
  var main_menu=$(".main-menu"),
      container=$('.container'),
      second_level=$(".second-level"),
      first_level_li=$(".first-level > li");
  main_menu.off('mouseenter','.first-level > li');
  main_menu.off('mouseleave','.first-level > li');
  if(container.width() > '300'){
    var menu_Width=0;
    second_level.hide().css({'height': 'auto'});
    first_level_li.each(function(){
      menu_Width += $(this).find('a').width()+36;
    });

   
    main_menu.css({
      'width': menu_Width+'px'
    });
    main_menu.on('mouseenter','.first-level > li', function(){
      var secon_level=$(this).children('.second-level'),
          m_l=Math.round((secon_level.width()-$(this).width())/2);
      secon_level.css({
          'height': 'auto',
          'margin-left': '-'+m_l+'px'          
        }).stop().slideToggle(100);
    });
    main_menu.on('mouseleave', '.first-level > li', function(){
      var secon_level = $(this).children('.second-level');
      secon_level.stop().slideToggle(100);
    }); 
  }else{
    main_menu.css({'width': '100%'});
    second_level.each(function(){
      $(this)
        .css({
          'height': 'auto',
          'margin-left': 'auto' 
        }).show();
    });
    return;
  }
}
function placeholder() { // placeholder for inputs textarea
  //placeholder for form    
  "use strict";
  var input_textarea=$('input, textarea');
  input_textarea.focus(function() {
    if($(this).data('placeholder') === $(this).val()){
      $(this).val('');  
    }
  });
  input_textarea.blur(function() {
    if ($(this).val() === '') {
      $(this).val($(this).data('placeholder'));
    }
  });     
}
$(window).resize(function(){
  "use strict";
  dropdown_menu();
});

$(document).ready(function(){
  "use strict";
  dropdown_menu();
  placeholder();
  var tabs_nav=$(".tabs-nav"),
      accord=$(".accord"),
      prettyPhoto_link=$("a[data-rel^='prettyPhoto']"),
      check_form_submit_btn=$('.check_form input[type="submit"]');
  //for Tabs
  // setup ul.tabs to work as tabs for each div directly under div.panes
  tabs_nav.tabs(".tabs-content > .tab-content");
  //for Toggles
  accord.tabs(
      ".accord .accordion-content",
      {tabs: 'h5', effect: 'slide', initialIndex: 1}
  );
  //pretty photo in portfolio use
  prettyPhoto_link.prettyPhoto({
    deeplinking : false,
    keyboard_shortcuts : false
  });

  //Form Validator

  check_form_submit_btn.click(function(){
    var form = $(this).closest('.check_form');
    form_validate(form);
    if(form.find(".check").hasClass("required")===true){
      return false;
    }else{
      form.submit();
    }
  });
});
